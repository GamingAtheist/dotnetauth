using System;
using System.Collections.Generic;
using DotNetAuth.OAuth2.Framework;

namespace DotNetAuth.OAuth2
{
    /// <summary>
    /// Provides basic methods to deal with authorization process using OAuth 2.0 protocol.
    /// </summary>
    public abstract class OAuth2ProviderDefinition
    {
        /// <summary>
        /// A URI in the resource server's domain in which at the first step of the authentication process user will be redirected to.
        /// When users are redirected to this URI, they can grant access to your app or decline. If they are not logged in to the 
        /// resource server they will be asked to login. If they are logged in and previously have authorized your app, they will
        /// be immediately redirected back to your app.
        /// </summary>
        public string AuthorizationEndpointUri { get; set; }
        /// <summary>
        /// A URI in the resource server's domain to make a server-side request to exchange a code with an access token. The code is 
        /// passed to your app by resource server when redirecting users to your site as a query string parameter. This code is accessible
        /// to user and anyone on the wire. You app is the only one who can exchange it for an access token because you know the application
        /// credential used to generate code and this credential will be checked before producing an access token.        
        /// </summary>
        public string TokenEndpointUri { get; set; }
        /// <summary>
        /// Indicates if the OAuth provider honours the state argument.
        /// </summary>
        /// <remarks>
        /// When implementing definition for a OAuth provider, you may override this property to specify the behaviour supported by current provider. If the provider does not return back the same state value after authentication, override and return false.
        /// </remarks>
        public virtual bool StateHonored { get { return true; } }
        /// <summary>
        /// Returns a list of parameters to be included in authorization endpoint as query string. 
        /// </summary>
        /// <remarks>
        /// When implementing a custom provider you need to override this method and fill the list of parameters according to the provider's documentation.
        /// However the default implementation provides a response for common OAuth parameters(e.g client_id,redirect_uri,scope,state,response_type)
        /// </remarks>
        /// <param name="appCredentials">The user's application credentials.</param>
        /// <param name="redirectUri">The redirect URI in which OAuth user wishes sites user to be returned to finally</param>
        /// <param name="scope">The scope of access or set of permissions OAuth user is demanding.</param>
        /// <param name="stateManager">An implementation of <see cref="IOAuth20StateManager"/> for providing state value.</param>
        /// <returns>A list of parameters to be included in authorization endpoint.</returns>
        public virtual ParameterSet GetAuthorizationRequestParameters(ApplicationCredentials appCredentials, string redirectUri, string scope, IOAuth20StateManager stateManager)
        {
            return new ParameterSet(new Dictionary<string, string>{ 
                {"client_id", appCredentials.AppId},
                {"redirect_uri", redirectUri},
                {"scope", scope},
                {"state", stateManager != null ? stateManager.GetState() : null},
                {"response_type", "code"},
            });
        }

        /// <summary>
        /// Returns a list of parameters to be included in request of exchanging code for access token.
        /// </summary>
        /// <param name="credentials">The code which passed to process end point by provider.</param>
        /// <param name="redirectUri">The very exact uri value passed to <see cref="GetAuthorizationRequestParameters"/> as redirect uri.</param>
        /// <param name="code">The value for code query string parameter whihc is passed to your application by provider.</param>
        /// <param name="grantType">The value for grant_type argument.</param>
        /// <returns>A list of parameters to be included in request of exchaning code for access token.</returns>
        public virtual ParameterSet GetExchangeCodeForAccessTokenParameters(ApplicationCredentials credentials, string redirectUri, string code, string grantType = "authorization_code")
        {
            return new ParameterSet(new Dictionary<string, string> {
                {"code", code},
                {"client_id", credentials.AppId},
                {"redirect_uri", redirectUri},
                {"client_secret", credentials.AppSecretId},
                {"grant_type", grantType}
            });
        }
        /// <summary>
        /// Parses the response body to request to access token.
        /// </summary>
        /// <param name="body">The response body of request for access token.</param>
        /// <returns>A <see cref="ProcessUserResponseOutput"/> object containing access token and some additional parameters or error details.</returns>
        public virtual ProcessUserResponseOutput ParseAccessTokenResult(string body)
        {
            var parsedResult = Newtonsoft.Json.Linq.JObject.Parse(body);
            var allParameters = new ParameterSet();
            foreach (var property in parsedResult.Properties())
                allParameters.Add(property.Name, parsedResult.Value<string>(property.Name));

            var accessToken = allParameters["access_token"];
            var expiresIn = allParameters["expires_in"];
            DateTime? expiresInDateTime = null;
            if (expiresIn != null) {
                var seconds = int.Parse(expiresIn);
                expiresInDateTime = DateTime.Now.AddSeconds(seconds);
            }
            var refreshToken = allParameters["refresh_token"];
            return new ProcessUserResponseOutput(allParameters, accessToken, expiresInDateTime, refreshToken);
        }
    }
}
