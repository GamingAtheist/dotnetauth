using System;
using System.Threading.Tasks;
using DotNetAuth.Common;
using DotNetAuth.OAuth2.Framework;

namespace DotNetAuth.OAuth2
{
    /// <summary>
    /// 
    /// </summary>
    /// <remarks>
    /// <example>
    /// <para>
    /// Asp.Net WebForms sample    
    /// <code>
    /// </code>    
    /// </para>
    /// </example>
    /// </remarks>
    public class OAuth2Process
    {
        /// <summary>
        /// In a server-flow as the first step of authentication process users should be redirected to a
        /// URI in which they can decide if they want grant access or not.
        /// This method returns that URI. Its your responsibility to redirect user to that URI.
        /// In an MVC application you may invoke Redirect method to produce ActionResult of an action.
        /// In a Web Forms application you may call Response.Redirect method.
        /// </summary>
        /// <param name="definition">An instance of an object representing an OAuth 2.0 service provider. You may 
        /// find a predefined implementation in the <see cref="DotNetAuth.OAuth2.Providers"/> namespace.</param>
        /// <param name="appCredentials">An object which represents your application's keys. To obtains keys you need to 
        /// register an application in the service provider's developers dashboard.</param>
        /// <param name="afterAuthenticationRedirectUri">The URI in which users will be returned back to once they
        /// decided about granting access to your application or not.</param>
        /// <param name="scope">A string to demand for certain access rights(scope of access rights). Each provider has it own rules on composing
        /// this scope parameter.</param>
        /// <param name="stateManager">The state manager.</param>
        /// <returns></returns>
        public static Uri GetAuthenticationUri(OAuth2ProviderDefinition definition, ApplicationCredentials appCredentials, string afterAuthenticationRedirectUri, string scope, IOAuth20StateManager stateManager)
        {
            var arguments = definition.GetAuthorizationRequestParameters(appCredentials, afterAuthenticationRedirectUri, scope, stateManager);
            var authenticationUri = ParametersUtil.CreateUri(definition.AuthorizationEndpointUri, arguments);
            return authenticationUri;
        }
        /// <summary>
        /// When user is redirected back to your website, checks if user has granted rights to your application and 
        /// then manages to retrieve access token.
        /// </summary>
        /// <param name="definition">An instance of an object representing an OAuth 2.0 service provider. You may 
        /// find a predefined implementation in the <see cref="DotNetAuth.OAuth2.Providers"/> namespace.</param>
        /// <param name="credentials">An object which represents your application's keys. To obtains keys you need to 
        /// register an application in the service provider's developers dashboard.</param>
        /// <param name="requestedUri">The uri in which user is redirected to your website. Usually <code>Request.Url</code> is all you must provide.</param>
        /// <param name="afterAuthenticationRedirectUri">The very exact uri value passed to <see cref="GetAuthenticationUri"/> with the same parameter name. </param>
        /// <param name="stateManager">The state manager.</param>
        /// <returns></returns>
        public static Task<ProcessUserResponseOutput> ProcessUserResponse(OAuth2ProviderDefinition definition, ApplicationCredentials credentials, Uri requestedUri, string afterAuthenticationRedirectUri, IOAuth20StateManager stateManager)
        {
            // Check if state in redirected URI matches the state initially passed to authentication endpoint
            string state = requestedUri.GetQueryArgument("state");
            var stateIsValid = stateManager == null || stateManager.CheckState(state);
            if (!stateIsValid)
                throw new InvalidOAuthStateException();

            #region Handle Error response
            var error = requestedUri.GetQueryArgument("error");
            if (!string.IsNullOrEmpty(error)) {
                var reason = requestedUri.GetQueryArgument("error_reason");
                var description = requestedUri.GetQueryArgument("error_description");
                return Task.Factory.StartNew(() => new ProcessUserResponseOutput(null, error, reason, description));
            }
            #endregion

            string code = requestedUri.GetQueryArgument("code");
            if (string.IsNullOrEmpty(code))
                throw new Exception("Something is wrong");

            var arguments = definition.GetExchangeCodeForAccessTokenParameters(credentials, afterAuthenticationRedirectUri, code);
            var http = new RestSharp.Http
                {
                    Url = new Uri(definition.TokenEndpointUri),
                    RequestBody = ParametersUtil.AsQueryString(arguments),
                    RequestContentType = "application/x-www-form-urlencoded"
                };
            var result = Task.Factory.StartNew(() =>
                {
                    var response = http.Post();
                    var body = response.Content;
                    var output = definition.ParseAccessTokenResult(body);
                    output.State = state;
                    return output;
                });
            return result;
        }
    }
}