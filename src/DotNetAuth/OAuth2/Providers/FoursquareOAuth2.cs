
namespace DotNetAuth.OAuth2.Providers
{
    /// <summary>
    /// OAuth 2 provider for Foursquare.
    /// </summary>
    public class FoursquareOAuth2 : OAuth2ProviderDefinition
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FoursquareOAuth2"/> class.
        /// </summary>
        public FoursquareOAuth2()
        {
            AuthorizationEndpointUri = "https://foursquare.com/oauth2/authenticate";
            TokenEndpointUri = "https://foursquare.com/oauth2/access_token";
        }
    }
}