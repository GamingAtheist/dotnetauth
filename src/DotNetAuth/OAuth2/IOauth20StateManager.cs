﻿using System;

namespace DotNetAuth.OAuth2
{
    /// <summary>
    /// An interface which allows saving state through OAuth 2 authentication process.
    /// </summary>
    /// <remarks>    
    /// <para>
    /// When using <see cref="OAuth2Process"/> methods you need to pass an instance of this interface. The <see cref="GetState"/> method
    /// will be called and gives you the opportunity to specify an arbitrary value as state. This state value will be passed back
    /// to you after user has been redirected back to your site.
    /// </para>
    /// <para>
    /// To simply pass ad-hoc functions instead of implementing this interface, use <see cref="Oauth20StateManager"/>. And even you may only pass the related function
    /// based on what function is being called. So pass a function for <see cref="GetState"/> when calling <see cref="OAuth2Process.GetAuthenticationUri"/> and pass
    /// a function for <see cref="CheckState"/> when calling <see cref="OAuth2Process.ProcessUserResponse"/>.
    /// <example>
    /// <para>The below code shows how to use <see cref="Oauth20StateManager"/></para>
    /// <code>
    /// OAuth2Process.GetAuthenticationUri(new GoogleOAuth2(), credentials, userResponseProcessUri, scope, new OAuth20StateManager(()=>"{return_url:'/account/data'}",null));
    /// </code>
    /// </example>
    /// </para>
    /// <para>
    /// <b>Note:</b> Some providers do not support state parameter. To see if a provider supports state parameter see <see cref="OAuth2ProviderDefinition.StateHonored"/>.
    /// </para>
    /// <para>
    /// <h4>OAuth 1.0a</h4>
    /// OAuth 1.0a also supports state management. However the process is different. The <see cref="DotNetAuth.OAuth1a.IOAuth10aStateManager"/> does 
    /// the same thing for OAuth 1.0a.
    /// </para>
    /// </remarks>
    public interface IOAuth20StateManager
    {
        /// <summary>
        /// You produce a value, a unique value maybe. A value you wish to be returned to you after user is returned back to your application.
        /// This method will be called by <see cref="OAuth2Process.GetAuthenticationUri"/> to add a state parameter to query string.
        /// </summary>
        /// <returns>An arbitrary value.</returns>
        string GetState();
        /// <summary>
        /// You will receive a value returned along with user's response. Supposedly this is a value you previously given to the provider
        /// using the return value of <see cref="GetState"/> method. You can check this value with your track of values returned from <see cref="GetState"/>
        /// method and associate the request to your after-authorization-endpoint with the original request.
        /// </summary>
        /// <remarks>
        /// This method will be called by <see cref="OAuth2Process.ProcessUserResponse"/> to give back the state to calling function.
        /// </remarks>
        /// <param name="stateCode">The state value. It should be a value which is previously supplied by <see cref="GetState"/> method.</param>
        /// <returns>If stateCode is valid returns true.</returns>
        bool CheckState(string stateCode);
    }

    /// <summary>
    /// A default implementation of IOauth20StateManager which allows you to specify your own methods for getting state and checking state.
    /// </summary>
    public class Oauth20StateManager : IOAuth20StateManager
    {
        readonly Func<string> getState;
        readonly Func<string, bool> checkState;
        /// <summary>
        /// Initializes a new instance of the <see cref="Oauth20StateManager"/> class.
        /// Yo don not have to pass both of the functions.
        /// </summary>
        /// <remarks>
        /// <para>
        /// When calling <see cref="OAuth2Process.GetAuthenticationUri"/> you may only pass a function for <paramref name="getStateFunc"/>. 
        /// And when calling <see cref="OAuth2Process.ProcessUserResponse"/> you may only pass a function for <paramref name="checkStateFunc"/>.
        /// </para>
        /// </remarks>
        /// <param name="getStateFunc">The get state function.</param>
        /// <param name="checkStateFunc">The check state function.</param>
        public Oauth20StateManager(Func<string> getStateFunc, Func<string, bool> checkStateFunc)
        {
            getState = getStateFunc;
            checkState = checkStateFunc;
        }
        /// <summary>
        /// The implementation of <see cref="IOAuth20StateManager.GetState"/>
        /// </summary>
        /// <returns></returns>
        public string GetState()
        {
            return getState();
        }
        /// <summary>
        /// The implementation of <see cref="IOAuth20StateManager.CheckState"/>
        /// </summary>
        /// <param name="stateCode"></param>
        /// <returns></returns>
        public bool CheckState(string stateCode)
        {
            return checkState(stateCode);
        }
    }
}
