using System;
using DotNetAuth.OAuth1a;
using NUnit.Framework;

namespace DotNetAuth.OAuth1.Framework.Tests
{
    public class OAuth10aStateManagerTest
    {
        [TestFixture]
        public class SaveTemporaryTokenSecretMethod
        {
            [Test]
            public void ShouldCallThePassedMethod()
            {
                bool saveCalled = false;
                Action<string, string> save = (state, secret) => {
                    saveCalled = true;
                };
                Func<string, string> load = state => "";
                var sut = new OAuth10aStateManager(save, load);
                sut.SaveTemporaryTokenSecret("", "");
                Assert.True(saveCalled);
            }
        }

        [TestFixture]
        public class LoadTemporaryTokenSecretMethod
        {
            [Test]
            public void ShouldCallThePassedMethod()
            {
                bool loadCalled = false;
                Action<string, string> save = (state, secret) => { };
                Func<string, string> load = state => {
                    loadCalled = true;
                    return "";
                };
                var sut = new OAuth10aStateManager(save, load);
                sut.LoadTemporaryTokenSecret("");
                Assert.True(loadCalled);
            }
        }
    }
}
