using System;
using DotNetAuth.OAuth1a.Framework;
using NUnit.Framework;

namespace DotNetAuth.OAuth1.Framework.Tests
{
    public class TimestampUtilTests
    {
        [TestFixture]
        public class GetTimeStampFrom1_1_1970Method
        {
            [Test]
            public void Results_a_TimeSpan_from_1_January_1970_til_Now_in_UTC()
            {
                var expected = DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1, 0, 0, 0));
                TimestampUtil.TimeDifferenceInSeconds = 0;
                var result = TimestampUtil.GetTimeStampFrom1_1_1970();
                Assert.AreEqual(expected.TotalSeconds, result.TotalSeconds, 1);
            }
        }
    }
}
