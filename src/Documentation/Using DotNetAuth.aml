﻿<?xml version="1.0" encoding="utf-8"?>
<topic id="0f5b36f8-c907-412d-80b4-3a5b74fe4bde" revisionNumber="1">
  <developerConceptualDocument
    xmlns="http://ddue.schemas.microsoft.com/authoring/2003/5"
    xmlns:xlink="http://www.w3.org/1999/xlink">   

    <introduction>
		<autoOutline />      
    </introduction>
    <section address="Section1">
      <title>Introduction</title>
      <content>
        <para>First of all make sure that you want to use DotNetAuth and not 
      		DotNetAuth Profiles. If you are not sure then you might need to read
      		<link xlink:href="2a0da54b-a678-410d-b70e-072dc85612d1" />. </para>
      	<para>
      		This documentation helps you find your way through implementing OAuth for your application.      		
      	</para>
      	<para>
      		Now that we have decided to use DotNetAuth we must know how it works actually and what is exact objective of using it.
      		OAuth is to give your application or website a key to access to users' data. That key is called access token.
      		So with DotNetAuth we aim at two primary objective:      		
      	</para>      	
		<list class="bullet">
		  <listItem><para>Get access tokens</para></listItem>
		  <listItem><para>Use access tokens</para></listItem>
		</list>
      </content>
    </section>
    <section address="UsingDotNetAuth_StepsInvolved">
      <title>Steps Involved</title>
      <content>      
      <para>
      	App Registration. OAuth authentication logic. Using access token. Managing access token.
      </para>
      </content>      
    </section>
    <section address="UsingDotNetAuth_GettingAccessTokens">
  	  <title>Getting access tokens</title>
  	  <content>
  	    <param>
  	    	You need to do some manual processes and develop some specific codes to get things working.  	    	
  	    </param>
		<list class="ordered">
		  <listItem><para>Register your application(website) within the provider.</para></listItem>
		  <listItem><para>Decide which version of OAuth the provider is using.</para></listItem>
		  <listItem><para>See if that provider has a default implementation in DotNetAuth</para></listItem>  
		  <listItem><para>If there is no default implementation, define yours. Ask us to implement it.</para></listItem>
		  <listItem><para>Consider specific pages or endpoints(whatever you name it) for handling interactions with providers.</para></listItem>  
		  <listItem><para>For when you don't have a valid access token, you need code to redirect user's agent to a specific page of provider. Based on version of OAuth, there is a method to give you the relevant url.</para></listItem>
		  <listItem><para>An endpoint(page,url) has to accept provider/user's response. Depending on version of OAuth there is a method to analyze/process the response and give you access token or in case of failure gives you failure reasons.</para></listItem>
		  <listItem><para>One you have the access token you might save it. Probably in association with current user.</para></listItem>
		  <listItem><para>You might also seek to see if an expiration details is provided to you. If so it is better to save that as well.</para></listItem>
		  <listItem><para>Make authenticated HTTP requests. Now you can use that access token to access users' data or do actions in behalf of them.</para></listItem>
		  <listItem><para>To use the access token you must pass it as part of your HTTP request. As part of query string or a HTTP HEADER.</para></listItem>
		  <listItem><para>Be ready for error message that says token is expired. Handle it appropriatly.</para></listItem>		  
		  <listItem><para>Renew access token if you know it is expired or you are afraid it might be expired.</para></listItem>		  
		</list>  	  
  	  </content>
    </section>
    <section address="UsingDotNetAuth_UsingAccessTokens">
  	  <title>Using access tokens</title>
  	  <content>
		<list class="ordered">
  			<listItem><para>Check if you already have a valid access token for the current user.</para></listItem>		
		    <listItem><para>If there is no access token or it is expired then follow the procedure.</para></listItem>
		</list>  	  
  	  </content>
    </section>
    <relatedTopics>
    </relatedTopics>
  </developerConceptualDocument>
</topic>
